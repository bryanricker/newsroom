Newsroom.js
========

A Node.js application that will help organize the editorial process in a high-activity CMS.
This project is a work-in-progress.

Tests are built using [Mocha](http://visionmedia.github.com/mocha/) and [Chai](http://chaijs.com/).
To run tests:

    make test
